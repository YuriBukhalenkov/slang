#ifndef ERROR_CODES_HEADER
#define ERROR_CODES_HEADER

enum EError
{
	Error_OK,
	Error_Syntax,
	Error_Unbound,  /* Attempted to evaluate a symbol for which no binding exists      */
	Error_Args,     /* A list expression was shorter or longer than expected           */
	Error_MissType, /* n object in an expression was of a different type than expected */
	Error_Bound,    /* attempted to bind already existed variable                      */
};

#endif /* ERROR_CODES_HEADER */