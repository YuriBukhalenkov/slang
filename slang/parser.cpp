#include "slang.h"
#include "parser.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>


static const char delim[] = ") \t\n\r";


static bool is_prefix( const char ch )
{
	return NULL != strchr( "()\'`", ch );
}

static const char* trim_left( const char* str )
{
	return str + strspn( str, " \t\n\r" );
}

static EError lex( const char* str, const char** start, const char** end )
{
	EError res = Error_OK;

	str = trim_left( str );

	while ( ';' == *str )
	{
		str += strcspn  ( str, "\n\r" );
		str  = trim_left( str );
	}

	if ( '\0' == *str )
	{
		*start = *end = NULL;
		res = Error_Syntax;
	}
	else if ( is_prefix(*str) )
	{
		*start = str;
		*end   = str + 1;
	}
	else if ( ',' == *str )
	{
		*start = str;
		*end   = str + ('@' == str[1] ? 2 : 1);
	}
	else if ( '\"' == *str )
	{
		// find pair quotes
		if ( NULL != (*end = strchr(str+1, '\"')) )
		{
			*start = str;
			*end   = *end + 1;
		}
		else
		{
			*start = NULL;
			res    = Error_Syntax;
		}
	}
	else
	{
		*start = str;
		*end   = str + strcspn( str, delim ); // extruct lexeme
	}

	return res;
}

/* detect number, atom, string or NIL */
static EError parse_simple( const char* start, const char* end, SCell& cell )
{
	char* p;

	/* it is a number */
	if ( isdigit(*start) || '-' == *start )
	{
		long ival = strtol( start, &p, 10 );
		if ( p == end )
		{
			cell = make_num( make_num(ival) );
			return Error_OK;
		}

		double fval = strtod( start, &p );
		if ( p == end )
		{
			cell = make_num( make_num(fval) );
			return Error_OK;
		}
	}
	/* string */
	else if ( '\"' == *start )
	{
		cell = make_string( start + 1, end - start - 2 );
		return Error_OK;
	}

	/* nil or atom */
	cell = 0 == strcmp( start, "nil" )
		? nil
		: make_atom( start, end - start );

	return Error_OK;
}

static EError read_list( const char* start, const char** end, SCell& cell )
{
	SCell in;

	*end = start;
	in   = cell = nil;

	for (;;)
	{
		const char* token;
		SCell       item;
		EError      err;

		if ( Error_OK != (err = lex(*end, &token, end)) )
		{
			return err;
		}

		if ( *token == ')' )
		{
			return Error_OK;
		}

		if ( *token == '.' && *end - token == 1 )
		{
			/* Improper list */
			if ( is_nil(in) )
				return Error_Syntax;

			if ( Error_OK != (err = read_expr(*end, end, item)) )
			{
				return err;
			}

			cdr(in) = item;

			/* Read the closing ')' */
			if ( Error_OK == (err = lex(*end, &token, end)) && *token != ')' )
			{
				err = Error_Syntax;
			}

			return err;
		}

		if ( Error_OK != (err = read_expr(token, end, item)) )
		{
			return err;
		}

		if ( is_nil(in) )
		{
			/* First item */
			in = cell = cons( item, nil );
		}
		else
		{
			in = cdr(in) = cons( item, nil );
		}
	}
}

EError read_expr( const char* input, const char** end, SCell& cell )
{
	const char *token, *quote = NULL;
	EError      err;

	if ( Error_OK != (err = lex(input, &token, end)) )
	{
		return err;
	}

	if      ( *token == '(' ) { return read_list( *end, end, cell ); }
	else if ( *token == ')' ) { return Error_Syntax; }
	else if ( *token == '\'') { quote = "quote";     }
	else if ( *token == '`' ) { quote = "quasiquote";}
	else if ( *token == ',' ) { quote = token[1] == '@' ? "unquote-splicing" : "unquote";}

	if ( quote )
	{
		cell = cons( make_atom(quote), cons(nil, nil) );
		return read_expr( *end, end, cadr(cell) );
	}

	return parse_simple( token, *end, cell );
}
