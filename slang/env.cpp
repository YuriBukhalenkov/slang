#include "slang.h"

// environment represented as list:
// (parent (identifier . value)...)
// where:
// parent - cell of a parent environment, top level env has nil as a parent
// identifier - value name
// value - stored value

//================= env functions

SCell env_create( const SCell& parent )
{
	return cons( parent, nil );
}

EError env_get( const SCell& env, const SCell& id, SCell& result )
{
	SCell parent = car( env );

	for ( SCell b, bs = cdr(env); !is_nil(bs); bs = cdr(bs) )
	{
		b = car( bs );
		if ( 0 == strcmp(car(b).value.atom, id.value.atom) )
		{
			result = cdr( b );
			return Error_OK;
		}
	}

	return is_nil(parent) ? Error_Unbound : env_get( parent, id, result );
}

EError env_set( SCell& env, const SCell& id, const SCell& value )
{
	SCell b = nil;

	for ( SCell bs = cdr(env); !is_nil(bs); bs = cdr(bs) )
	{
		b = car(bs);
		if ( 0 == strcmp(car(b).value.atom, id.value.atom) )
		{
			return Error_Bound;
		}
	}

	// construct new binding and push it to front
	b = cons( id, value );
	cdr(env) = cons(b, cdr(env));

	return Error_OK;
}