#ifndef SLANG_NUM_HEADER
#define SLANG_NUM_HEADER

/* number structure */
struct SNum 
{
     bool is_fix;
     union 
     {
          long   i; // (i)nt
          double f; // (f)loat
     } value;
};

//================= number routines
static inline SNum make_num( long n )
{
	SNum num;
	num.is_fix  = true;
	num.value.i = n;
	return num;
}

static inline SNum make_num( double n )
{
	SNum num;
	num.is_fix  = false;
	num.value.f = n;
	return num;
}

static inline double num_as_float( const SNum& n )
{
	return n.is_fix ? (double)n.value.i : n.value.f;
}

static inline long num_as_int( const SNum& n )
{
	return n.is_fix ? n.value.i : (long)n.value.f;
}

static inline bool is_real( const SNum& n )
{
	return !n.is_fix;
}

SNum num_add( const SNum& a, const SNum& b );
SNum num_sub( const SNum& a, const SNum& b );
SNum num_mul( const SNum& a, const SNum& b );
SNum num_div( const SNum& a, const SNum& b );
SNum num_rem( const SNum& a, const SNum& b );

bool num_eq ( const SNum& a, const SNum& b );
bool num_gt ( const SNum& a, const SNum& b );
bool num_lt ( const SNum& a, const SNum& b );
bool num_ge ( const SNum& a, const SNum& b );
bool num_le ( const SNum& a, const SNum& b );

#endif /* SLANG_NUM_HEADER */