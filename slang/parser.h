#ifndef PARSER_HEADER
#define PARSER_HEADER

#include "error_codes.h"

struct SCell;

EError read_expr( const char* input, const char** end, SCell& cell );


#endif /* PARSER_HEADER */