#ifndef ENV_HEADER
#define ENV_HEADER

#include "error_codes.h"

struct SCell;

SCell  env_create( const SCell& parent );
EError env_get   ( const SCell& env, const SCell& cell, SCell& result );
EError env_set   ( SCell& env, const SCell& cell, const SCell& value );

#endif /* ENV_HEADER */