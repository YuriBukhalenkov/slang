#include "slang.h"
#include "list.h"
#include "parser.h"

typedef bool(*compare_fn)(const SNum&, const SNum&);
typedef SNum(*arithmetic_fn)(const SNum&, const SNum&);
typedef bool(*type_fn)(const SCell&);
typedef bool(*pair_compare_fn)(const SCell&, const SCell&);

/* basic section */

EError builtin_car( const SCell& args, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	if ( is_nil(car(args)) )
		result = nil;
	else if ( !is_pair(car(args)) )
		return Error_MissType;
	else
		result = caar(args);

	return Error_OK;
}

EError builtin_cdr( const SCell& args, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	if ( is_nil(car(args)) )
		result = nil;
	else if ( !is_pair(car(args)) )
		return Error_MissType;
	else
		result = cdar(args);

	return Error_OK;
}

EError builtin_cons( const SCell& args, SCell& result )
{
	if ( is_nil(args) || is_nil(cdr(args)) || !is_nil(cddr(args)) )
		return Error_Args;

	result = cons(car(args), cadr(args));

	return Error_OK;
}

EError builtin_read_expr( const SCell& args, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	if ( !is_string(car(args)) )
		return Error_MissType;

	const char* p = car(args).value.string;

	return read_expr( p, &p, result );
}

EError builtin_length( const SCell& args, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	const SCell item = car(args);
	const long  len  = is_string(item)
		? strlen(item.value.string)
		: list_length(item);

	result = make_num( make_num(len) );

	return Error_OK;
}

EError builtin_apply( const SCell& args, SCell& result )
{
	if ( is_nil(args) || is_nil(cdr(args)) || !is_nil(cddr(args)) )
		return Error_Args;

	SCell fn_body = car( args ),
		  fn_args = cadr( args );

	return is_list(fn_args) ? apply(fn_body, fn_args, result) : Error_Syntax;
}

static bool pair_eq( const SCell& a, const SCell& b )
{
	bool res = false;

	if ( a.type == b.type )
	{
		switch ( a.type )
		{
		case SCell::NIL:
			res = true;
			break;
		case SCell::PAIR:
		case SCell::LAMBDA:
		case SCell::MACRO:
			res =  pair_eq(car(a), car(b))
				&& pair_eq(cdr(a), cdr(b));
			break;
		case SCell::ATOM:
		case SCell::STRING:
			res = 0 == strcmp(a.value.atom, b.value.atom);
			break;
		case SCell::NUMBER:
			res = num_eq(a.value.num, b.value.num);
			break;
		case SCell::BUILTIN:
			res = a.value.builtin == b.value.builtin;
			break;
		}
	}

	return res;
}

static bool pair_lt( const SCell& a, const SCell& b )
{
	bool res = a.type < b.type;

	if ( a.type == b.type )
	{
		switch ( a.type )
		{
		case SCell::NIL:
			res = false;
			break;
		case SCell::PAIR:
		case SCell::LAMBDA:
		case SCell::MACRO:
			res =  pair_lt(car(a), car(b))
				&& pair_lt(cdr(a), cdr(b));
			break;
		case SCell::ATOM:
		case SCell::STRING:
			res = 0 > strcmp(a.value.atom, b.value.atom);
			break;
		case SCell::NUMBER:
			res = num_lt(a.value.num, b.value.num);
			break;
		case SCell::BUILTIN:
			res = a.value.builtin < b.value.builtin; // TODO: ???
			break;
		}
	}

	return res;
}

static EError pair_compare( const SCell& args, SCell& result, pair_compare_fn fn )
{
	if ( is_nil(args) || is_nil(cdr(args)) || !is_nil(cddr(args)) )
		return Error_Args;

	result = (*fn)(car(args), cadr(args))
		? true_atom
		: false_atom;

	return Error_OK;
}

EError builtin_is_eq( const SCell& args, SCell& result )
{
	return pair_compare( args, result, &pair_eq );
}

EError builtin_is_lt( const SCell& args, SCell& result )
{
	return pair_compare( args, result, &pair_lt );
}


/* number arithmetic section */

static EError num_arithmetic( const SCell& args, SCell& result, arithmetic_fn fn )
{
	if ( is_nil(args) )
		return Error_Args;

	if ( !is_num(car(args)) )
		return Error_MissType;

	SNum res = car(args).value.num;

	for ( SCell p = cdr(args); !is_nil(p); p = cdr(p) )
	{
		if ( !is_num(car(p)) )
			return Error_MissType;

		res = (*fn)(res, car(p).value.num);
	}

	result = make_num(res);

	return Error_OK;
}

EError builtin_add( const SCell& args, SCell& result )
{
	return num_arithmetic( args, result, &num_add );
}

EError builtin_sub( const SCell& args, SCell& result )
{
	return (!is_nil(args) && is_nil(cdr(args)))
		? result = make_num( num_sub(make_num(0L), car(args).value.num) ), Error_OK
		: num_arithmetic( args, result, &num_sub );
}

EError builtin_mul( const SCell& args, SCell& result )
{
	return num_arithmetic( args, result, &num_mul );
}

EError builtin_div( const SCell& args, SCell& result )
{
	return (!is_nil(args) && is_nil(cdr(args)))
		? result = make_num( num_div(make_num(1L), car(args).value.num) ), Error_OK
		: num_arithmetic( args, result, &num_div );
}

EError builtin_rem( const SCell& args, SCell& result )
{
	return (is_nil(args) || is_nil(cdr(args)))
		? Error_Args
		: num_arithmetic( args, result, &num_rem );
}


/* number compare section */

static EError num_compare( const SCell& args, SCell& result, compare_fn fn )
{
	if ( is_nil(args) )
		return Error_Args;

	SCell p = car(args);

	if ( !is_num(p) )
		return Error_MissType;

	SNum res = p.value.num;

	for ( SCell p = cdr(args); !is_nil(p); p = cdr(p) )
	{
		if ( !is_num(car(p)) )
			return Error_MissType;

		if ( !(*fn)(res, car(p).value.num) )
		{
			result = false_atom;
			return Error_OK;
		}

		res = car(p).value.num;
	}

	result = true_atom;
	return Error_OK;
}

EError builtin_numlt( const SCell& args, SCell& result )
{
	return num_compare( args, result, &num_lt );
}

EError builtin_numgt( const SCell& args, SCell& result )
{
	return num_compare( args, result, &num_gt );
}

EError builtin_numle( const SCell& args, SCell& result )
{
	return num_compare( args, result, &num_le );
}

EError builtin_numge( const SCell& args, SCell& result )
{
	return num_compare( args, result, &num_ge );
}

EError builtin_numeq( const SCell& args, SCell& result )
{
	return num_compare( args, result, &num_eq );
}


/* type predicates section */

static EError is_type( const SCell& args, SCell& result, type_fn fn )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	result = (*fn)(car(args)) ? true_atom : false_atom;

	return Error_OK;
}

EError builtin_is_list( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_list );
}

EError builtin_is_nil( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_nil );
}

EError builtin_is_atom( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_atom );
}

static bool is_fun( const SCell& x )
{
	return is_builtin( x ) || is_lambda( x );
}

EError builtin_is_fun( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_fun );
}

EError builtin_is_num( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_num );
}

EError builtin_is_pair( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_pair );
}

EError builtin_is_string( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_string );
}

static bool is_integer( const SCell& x )
{
	return is_num( x ) && !is_real( x.value.num );
}

static bool is_real( const SCell& x )
{
	return is_num( x ) && is_real( x.value.num );
}

EError builtin_is_int( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_integer );
}

EError builtin_is_real( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_real );
}

EError builtin_is_macro( const SCell& args, SCell& result )
{
	return is_type( args, result, &is_macro );
}
