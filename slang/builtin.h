#ifndef BUILIN_HEADER
#define BUILIN_HEADER

struct SCell;

EError builtin_car ( const SCell& args, SCell& result );
EError builtin_cdr ( const SCell& args, SCell& result );
EError builtin_cons( const SCell& args, SCell& result );

EError builtin_read_expr( const SCell& args, SCell& result );

EError builtin_add( const SCell& args, SCell& result );
EError builtin_sub( const SCell& args, SCell& result );
EError builtin_mul( const SCell& args, SCell& result );
EError builtin_div( const SCell& args, SCell& result );
EError builtin_rem( const SCell& args, SCell& result );
EError builtin_numeq( const SCell& args, SCell& result );
EError builtin_numlt( const SCell& args, SCell& result );
EError builtin_numgt( const SCell& args, SCell& result );
EError builtin_numle( const SCell& args, SCell& result );
EError builtin_numge( const SCell& args, SCell& result );

EError builtin_is_list( const SCell& args, SCell& result );
EError builtin_is_nil ( const SCell& args, SCell& result );
EError builtin_is_atom( const SCell& args, SCell& result );
EError builtin_is_fun ( const SCell& args, SCell& result );
EError builtin_is_num ( const SCell& args, SCell& result );
EError builtin_is_int ( const SCell& args, SCell& result );
EError builtin_is_real( const SCell& args, SCell& result );
EError builtin_is_pair( const SCell& args, SCell& result );
EError builtin_is_string( const SCell& args, SCell& result );
EError builtin_is_macro( const SCell& args, SCell& result );
EError builtin_is_eq  ( const SCell& args, SCell& result );
EError builtin_is_lt  ( const SCell& args, SCell& result );

EError builtin_length( const SCell& args, SCell& result );
EError builtin_apply ( const SCell& args, SCell& result );

#endif /* BUILIN_HEADER */