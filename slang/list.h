#ifndef LIST_HEADER
#define LIST_HEADER

#include "slang.h"


//=====================================
// list functions

static SCell list_copy( SCell list )
{
	SCell a, p;

	if ( is_nil(list) )
		return nil;

	p = a = cons(car(list), nil);

	for ( list = cdr(list); !is_nil(list); list = cdr(list) )
	{
		cdr(p) = cons(car(list), nil);
		p      = cdr(p);
	}

	return a;
}

static unsigned int list_length( const SCell& list )
{
	unsigned int len = 0;

	for ( SCell p = list; !is_nil(p); p = cdr(p), ++len )
	{
		if ( !is_pair(p) )
			return 0;
	}

	return len;
}

static SCell list_reverse( SCell list )
{
	SCell reversed = nil;

	for ( ; !is_nil(list); list = cdr(list) )
	{
		reversed = cons(car(list), reversed);
	}

	return reversed;
}


#endif /* LIST_HEADER */