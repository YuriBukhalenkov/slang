#ifndef SLANG_HEADER
#define SLANG_HEADER

#include "num.h"
#include <string.h>
#include "error_codes.h"

typedef EError (*Builtin)(const struct SCell& args, struct SCell& result);

EError eval_expr( const SCell& expr, SCell& env, SCell& result );
EError apply( const SCell& fn, const SCell& args, SCell& result );

extern const struct SCell nil;
extern const struct SCell false_atom;
extern const struct SCell true_atom;


//================= struct SCell
struct SCell
{
	enum EType
	{
		NIL,
		ATOM,
		NUMBER,
		STRING,
		PAIR,
		BUILTIN,
		LAMBDA,
		MACRO,
	};

	union UValue
	{
		struct SPair* pair;
		const char*   atom;
		const char*   string;
		SNum          num;
		Builtin       builtin;
	};

	EType  type;
	UValue value;
};


//================= struct SPair
struct SPair
{
	SCell cell[2];
};


static inline SCell& car( SCell& x )
{
	return x.value.pair->cell[0];
}

static inline const SCell& car( const SCell& x )
{
	return x.value.pair->cell[0];
}

static inline SCell& cdr( SCell& x )
{
	return x.value.pair->cell[1];
}

static inline const SCell& cdr( const SCell& x )
{
	return x.value.pair->cell[1];
}


/* type checks */

static inline bool is_nil( const SCell& x )
{
	return SCell::NIL == x.type;
}

static inline bool is_pair( const SCell& x )
{
	return SCell::PAIR == x.type;
}

static inline bool is_list( const SCell& x )
{
	for ( SCell p = x; !is_nil(p); p = cdr(p) )
	{
		if ( !is_pair(p) )
			return false;
	}

	return true;
}

static inline bool is_atom( const SCell& x )
{
	return SCell::ATOM == x.type;
}

static inline bool is_builtin( const SCell& x )
{
	return SCell::BUILTIN == x.type;
}

static inline bool is_lambda( const SCell& x )
{
	return SCell::LAMBDA == x.type;
}

static inline bool is_num( const SCell& x )
{
	return SCell::NUMBER == x.type;
}

static inline bool is_string( const SCell& x )
{
	return SCell::STRING == x.type;
}

static inline bool is_macro( const SCell& x )
{
	return SCell::MACRO == x.type;
}


/* values constructing */

static SCell cons( const SCell& car_val, const SCell& cdr_val )
{
	SCell x = {SCell::PAIR};
	
	x.value.pair = new SPair;
	car(x) = car_val;
	cdr(x) = cdr_val;

	return x;
}

static SCell make_num( const SNum& num )
{
	SCell x = {SCell::NUMBER};
	x.value.num = num;
	return x;
}

static SCell make_atom( const char* s, const unsigned int n = -1 )
{
	SCell x = {SCell::ATOM};
	x.value.atom = n == (unsigned int)-1 ? strdup(s) : strndup(s, n);
	return x;
}

static SCell make_string( const char* s, const unsigned int n )
{
	SCell x = {SCell::STRING};
	x.value.string = strndup(s, n);
	return x;
}

static SCell make_builtin( Builtin fn )
{
	SCell c = {SCell::BUILTIN};
	c.value.builtin = fn;
	return c;
}

static EError make_lambda( const SCell& env, const SCell& args, const SCell& body, SCell& result )
{
	if ( !is_list(body) )
		return Error_Syntax;

	/* Check argument names are all symbols */
	for ( SCell p = args; !is_nil(p); p = cdr(p) )
	{
		/* variadic function */
		if ( is_atom(p) )
			break;
		else if ( !is_pair(p) || !is_atom(car(p)) )
			return Error_MissType;
	}

	/* lambda represented as: (env (arg...) expr...) */
	/* TODO (guess): multi-arity function (env (arg1...) expr1... (arg2...) expr2...) */
	result = cons( env, cons(args, body) );
	result.type = SCell::LAMBDA;

	return Error_OK;
}


/* some helpers */
template <typename T>
static inline T& cadr( T& x ) { return car(cdr(x)); }

template <typename T>
static inline T& cdar( T& x ) { return cdr(car(x)); }

template <typename T>
static inline T& cddr( T& x ) { return cdr(cdr(x)); }

template <typename T>
static inline T& caar( T& x ) { return car(car(x)); }

template <typename T>
static inline T& caddr( T& x ) { return car(cdr(cdr(x))); }

template <typename T>
static inline T& cdddr( T& x ) { return cdr(cdr(cdr(x))); }


#endif /* SLANG_HEADER */