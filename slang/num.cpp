#include "num.h"
#include <math.h>

//================= number routines implementation
SNum num_add( const SNum& a, const SNum& b )
{
	SNum ret;
	ret.is_fix = a.is_fix && b.is_fix;

	if ( ret.is_fix )
	{
		ret.value.i = a.value.i + b.value.i;
 	} 
 	else 
 	{
    	ret.value.f = num_as_float(a) + num_as_float(b);
    }

    return ret;
}

SNum num_sub( const SNum& a, const SNum& b )
{
	SNum ret;
	ret.is_fix = a.is_fix && b.is_fix;
 	if ( ret.is_fix ) 
 	{
    	ret.value.i = a.value.i - b.value.i;
 	} 
 	else 
 	{
    	ret.value.f = num_as_float(a) - num_as_float(b);
 	}
 	
 	return ret;
}

SNum num_mul( const SNum& a, const SNum& b ) 
{
	SNum ret;
	ret.is_fix = a.is_fix && b.is_fix;

	if ( ret.is_fix )
	{
		ret.value.i = a.value.i * b.value.i;
 	} 
 	else 
 	{
    	ret.value.f = num_as_float(a) * num_as_float(b);
 	}

	return ret;
}

SNum num_div( const SNum& a, const SNum& b )
{
	SNum ret;
 	ret.is_fix  = false;
   	ret.value.f = num_as_float(a) / num_as_float(b);

 	return ret;
}

SNum num_rem( const SNum& a, const SNum& b )
{
	SNum ret;
	ret.is_fix = a.is_fix && b.is_fix;

	if ( ret.is_fix )
	{
		ret.value.i = a.value.i % b.value.i;
	}
	else
	{
		ret.value.f = fmod( num_as_float(a), num_as_float(b) );
	}

	return ret;
}

bool num_eq( const SNum& a, const SNum& b ) 
{
	bool ret;
 	
 	if ( a.is_fix && b.is_fix )
 	{
    	ret = a.value.i == b.value.i;
    } 
    else 
    {
    	ret = num_as_float(a) == num_as_float(b);
    }

    return ret;
}

bool num_gt( const SNum& a, const SNum& b )
{
	bool ret = false;
 	
 	if ( a.is_fix && b.is_fix ) 
 	{
    	ret = a.value.i > b.value.i;
 	} 
 	else 
 	{
    	ret = num_as_float(a) > num_as_float(b);
 	}

 	return ret;
}

bool num_lt( const SNum& a, const SNum& b ) 
{
	bool ret = false;

	if( a.is_fix && b.is_fix ) 
	{
    	ret = a.value.i < b.value.i;
 	} 
 	else 
 	{
    	ret = num_as_float(a) < num_as_float(b);
 	}

 	return ret;
}

bool num_ge( const SNum& a, const SNum& b ) 
{
	return !num_lt(a, b);
}

bool num_le( const SNum& a, const SNum& b ) 
{
	return !num_gt(a, b);
}
