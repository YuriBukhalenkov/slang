#include "slang.h"
#include "env.h"
#include "list.h"
#include <dlfcn.h>


const SCell nil        = {SCell::NIL};
const SCell false_atom = nil;
const SCell true_atom  = make_atom( "#t" );

typedef EError (*fnSpForm)(const SCell& args, SCell& env, SCell& result);

static fnSpForm find_form( const SCell& op );


//================= core functions

EError apply( const SCell& fn, const SCell& args, SCell& result )
{
	if ( is_builtin(fn) )
		return (*fn.value.builtin)(args, result);
	else if ( !is_lambda(fn) )
		return Error_MissType;

	SCell env = env_create(car(fn)),
		_args = args,
		body  = cddr(fn);

	/* Bind args */
	for ( SCell arg_names = cadr(fn); !is_nil(arg_names); arg_names = cdr(arg_names), _args = cdr(_args) )
	{
		if ( is_atom(arg_names) )
		{
			env_set( env, arg_names, _args );
			_args = nil;
			break;
		}

		if ( is_nil(arg_names) )
			return Error_Args;

		env_set( env, car(arg_names), car(_args) );
	}

	if ( !is_nil(_args) )
		return Error_Args;

	for ( EError err = Error_OK; !is_nil(body); body = cdr(body) )
	{
		if ( Error_OK != (err = eval_expr(car(body), env, result)) )
			return err;
	}

	return Error_OK;
}

EError eval_expr( const SCell& expr, SCell& env, SCell& result )
{
	SCell  op, args;
	EError err;

	if ( is_atom(expr) )
	{
#ifdef _DEBUG
		if ( 0 == strcmp(expr.value.atom, "env") )
		{
			result = env;
			return Error_OK;
		}
#endif // DEBUG
		return env_get(env, expr, result );
	}
	else if ( !is_pair(expr) )
	{
		result = expr;
		return Error_OK;
	}

	if ( !is_list(expr) )
		return Error_Syntax;

	op   = car(expr);
	args = cdr(expr);

	if ( is_atom(op) )
	{
		fnSpForm fn = find_form( op );
		if ( fn )
		{
			return (*fn)( args, env, result );
		}
		else
		{
			if ( Error_OK != (err = env_get(env, op, op)) )
				return err;
		}
	}

	/* Evaluate operator */
	if ( Error_OK != (err = eval_expr(op, env, op)) )
		return err;

	if ( is_macro(op) )
	{
		SCell expansion;
		op.type = SCell::LAMBDA;

		if ( Error_OK != (err = apply(op, args, expansion)) )
			return err;

		return eval_expr( expansion, env, result );
	}

	/* Evaluate arguments */
	args = list_copy( args );
	for ( SCell p = args; !is_nil(p); p = cdr(p) )
	{
		if ( Error_OK != (err = eval_expr(car(p), env, car(p))) )
			return err;
	}

	return apply( op, args, result );
}


//================= special forms

// (def var exp)
static EError form_def( const SCell& args, SCell& env, SCell& result )
{
	if ( is_nil(args) || is_nil(cdr(args)) || !is_nil(cddr(args)) )
		return Error_Args;

	SCell sym = car(args), val;

	if ( !is_atom(sym) )
		return Error_MissType;

	EError err = eval_expr( cadr(args), env, val );

	if ( Error_OK == err )
	{
		result = sym;
		err    = env_set( env, sym, val );
	}

	return err;
}

// (defmacro (name args) body)
static EError form_defmacro( const SCell& args, SCell& env, SCell& result )
{
	if ( is_nil(args) || is_nil(cdr(args)) )
		return Error_Args;

	if ( !is_pair(car(args)) )
		return Error_Syntax;

	SCell macro,
		  name = caar(args);

	if ( !is_atom(name) )
		return Error_MissType;

	EError err = make_lambda( env, cdar(args), cdr(args), macro );

	if ( Error_OK == err )
	{
		macro.type = SCell::MACRO;
		result     = macro;
		err        = env_set( env, name, macro );
	}

	return err;
}

// (fun (var*) exp)
static EError form_fun( const SCell& args, SCell& env, SCell& result )
{
	return is_nil(args) || is_nil(cdr(args))
		? Error_Args
		: make_lambda( env, car(args), cdr(args), result );
}

// (do expr1 expr2 ... expr-n)
static EError form_do( const SCell& args, SCell& env, SCell& result )
{
	EError err = Error_Syntax;

	for ( SCell p = args; !is_nil(p); p = cdr(p) )
	{
		if ( Error_OK != (err = eval_expr(car(p), env, result)) )
			break;
	}

	return err;
}

// (if test true-expr [false-expr])
static EError form_if( const SCell& args, SCell& env, SCell& result )
{
	const unsigned int nArgsLen = list_length( args );

	if ( nArgsLen < 2 || nArgsLen > 3 )
		return Error_Args;

	SCell cond, val;
	EError err;

	if ( Error_OK != (err = eval_expr(car(args), env, cond)) )
		return err;

	if ( 3 == nArgsLen )
	{
		val = is_nil(cond) ? caddr(args) : cadr(args);
		err = eval_expr( val, env, result );
	}
	else if ( is_nil(cond) )
	{
		result = nil;
		err    = Error_OK;
	}
	else
	{
		err = eval_expr( cadr(args), env, result );
	}

	return err;
}

// (cond test-1 expr-1 [& test-n expr-n])
static EError form_cond( const SCell& args, SCell& env, SCell& result )
{
	const unsigned int nArgsLen = list_length( args );

	if ( nArgsLen & 1 )
		return Error_Args;

	EError err = Error_OK;
	result     = nil;

	for ( SCell p = args, res = nil; !is_nil(p); p = cddr(p) )
	{
		if ( Error_OK != (err = eval_expr(car(p), env, res)) )
			return err;

		if ( !is_nil(res) )
		{
			return eval_expr( cadr(p), env, result );
		}
	}

	return err;
}

// (and expr1 expr2 ... expr-n)
static EError form_and( const SCell& args, SCell& env, SCell& result )
{
	EError err = Error_OK;
	result     = true_atom;

	for ( SCell p = args; !is_nil(p) && !is_nil(result); p = cdr(p) )
	{
		if ( Error_OK != (err = eval_expr(car(p), env, result)) )
			break;
	}

	return err;
}

// (or expr1 expr2 ... expr-n)
static EError form_or( const SCell& args, SCell& env, SCell& result )
{
	EError err = Error_OK;
	result     = false_atom;

	for ( SCell p = args; !is_nil(p); p = cdr(p) )
	{
		if ( Error_OK != (err = eval_expr(car(p), env, result)) || !is_nil(result) )
			break;
	}

	return err;
}

// (quote exp)
static EError form_quote( const SCell& args, SCell& env, SCell& result )
{
	return is_nil(args) || !is_nil(cdr(args))
		? Error_Args
		: (result = car(args), Error_OK);
}

// NOTE: does't work properly
// (quasiquote [expr|(unquote expr2)|(unquote-splicing expr3)])
static EError form_quasiquote( const SCell& args, SCell& env, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	SCell _args = car(args);

	for ( SCell p = _args; !is_nil(p); p = cdr(p) )
	{
		SCell v = car(p);

		if ( is_pair(v) )
		{
			if ( is_atom(car(v)) && 0 == strncmp(car(v).value.atom, "unquote", 7) )
			{
				SCell  unquoted;
				EError err;

				if ( Error_OK != (err = eval_expr(cadr(v), env, unquoted)) )
					return err;

				if ( 0 == strcmp(car(v).value.atom+7, "-splicing") )
				{
				}

				car(p) = unquoted;
			}
		}
	}

	result = _args;
	return Error_OK;
}

// (macroexpand macro)
static EError form_macroexpand( const SCell& args, SCell& env, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	SCell op  = caar( args ),
		_args = cdar( args ),
		macro;

	EError err;

	if ( Error_OK != (err = env_get(env, op, macro)) )
		return err;

	if ( !is_macro(macro) )
		return Error_MissType;

	macro.type = SCell::LAMBDA;

	return apply( macro, _args, result );
}

// (bind builtin-fun-name)
static EError form_bind( const SCell& args, SCell& env, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	if ( !is_string(car(args)) )
		return Error_MissType;

	Builtin builtin = (Builtin)dlsym( RTLD_MAIN_ONLY, car(args).value.string );

	return builtin
		? result = make_builtin(builtin), Error_OK
		: Error_Unbound;
}

// TODO: времянка
bool load_lib( SCell& env, const char* path );

// (load module-path)
static EError form_load( const SCell& args, SCell& env, SCell& )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	if ( !is_string(car(args)) )
		return Error_MissType;

	return load_lib( env, car(args).value.string ) ? Error_OK : Error_MissType;
}


//================= find special form function
static inline bool is_form( const SCell& op, const char* s )
{
	return 0 == strcmp( op.value.atom, s );
}

static fnSpForm find_form( const SCell& op )
{
	fnSpForm fn = NULL;

	if      ( is_form(op, "quote"   )) { fn = &form_quote;   }
	else if ( is_form(op, "defmacro")) { fn = &form_defmacro;}
	else if ( is_form(op, "if"      )) { fn = &form_if;      }
	else if ( is_form(op, "cond"    )) { fn = &form_cond;    }
	else if ( is_form(op, "def"     )) { fn = &form_def;     }
	else if ( is_form(op, "fun"     )) { fn = &form_fun;     }
	else if ( is_form(op, "and"     )) { fn = &form_and;     }
	else if ( is_form(op, "or"      )) { fn = &form_or;      }
	else if ( is_form(op, "do"      )) { fn = &form_do;      }
	else if ( is_form(op, "macroexpand")){fn = &form_macroexpand;}
	else if ( is_form(op, "bind"    )) { fn = &form_bind;    }
	else if ( is_form(op, "load"    )) { fn = &form_load;    }

	return fn;
}
