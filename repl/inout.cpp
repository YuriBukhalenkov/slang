#include <stdio.h>
#include "slang.h"
#include "list.h"


char* slurp( const char* path, unsigned long* read /* = NULL */ )
{
	FILE* file;
	char* buf = NULL;
	long  len;

	if ( NULL != (file = fopen(path, "r")) )
	{
		fseek( file, 0, SEEK_END );
		len = ftell( file );
		fseek( file, 0, SEEK_SET );

		if ( NULL != (buf = new char[len + 1]) )
		{
			const int read = fread( buf, 1, len, file );
			buf[read] = 0;
		}

		fclose( file );

		if ( read )
		{
			*read = len;
		}
	}

	return buf;
}


/* printing expression */
void print_expr( const SCell& x )
{
	switch ( x.type )
	{
	case SCell::NIL   : printf( "nil" ); break;
	case SCell::ATOM  : fputs( x.value.atom, stdout ); break;
	case SCell::STRING: printf( "\"%s\"", x.value.string ); break;

	case SCell::PAIR:
	{
		putchar( '(' );
		print_expr( car(x) );

		SCell cell = cdr( x );

		while ( !is_nil(cell) )
		{
			if ( SCell::PAIR == cell.type )
			{
				putchar( ' ' );
				print_expr( car(cell) );
				cell = cdr( cell );
			}
			else
			{
				printf( " . " );
				print_expr( cell );
				break;
			}
		}

		putchar( ')' );
		break;
	}

	case SCell::NUMBER:
		if ( is_real(x.value.num) )
			printf( "%f", num_as_float(x.value.num) );
		else
			printf( "%ld", num_as_int(x.value.num) );
		break;

	case SCell::BUILTIN: printf( "#<builtin:%p>", x.value.builtin ); break;
	case SCell::MACRO  :
		if ( is_list(cadr(x)) )
			printf( "MACRO/%u", list_length(cadr(x)) );
		else
			fputs( "#<MACRO/VAR>", stdout );
		break;
	case SCell::LAMBDA :
		if ( is_list(cadr(x)) )
			printf( "#<lambda/%u>", list_length(cadr(x)) );
		else
			fputs( "#<lambda/VAR>", stdout );
		break;
	} /* switch */
}

EError builtin_print( const SCell& args, SCell& result )
{
	if ( is_nil(args) )
		return Error_Args;

	bool res = false;

	// single argument
	if ( is_nil(cdr(args)) )
	{
		if ( is_string(car(args)) )
			fputs( car(args).value.string, stdout );
		else
			print_expr( car(args) );

		res = true;
	}
	// two arguments: 1-st(string), 2-nd (list)
	else if ( is_string(car(args)) && !is_nil(cdr(args)) && is_list(cadr(args)) && is_nil(cddr(args)) )
	{
		bool got_escape_char = false;
		const char* sfmt     = car(args).value.string;
		const char* p        = sfmt;
		SCell param          = cadr(args);

		for ( ; p && *p; ++p )
		{
			if ( !got_escape_char && '~' == *p )
			{
				got_escape_char = true;
			}
			else if ( got_escape_char )
			{
				printf( "%.*s", (int)(p - sfmt - 1), sfmt );

				if ( '@' == *p )
				{
					print_expr( car(param) );
					param = cdr(param);
					sfmt = p + 1;
				}
				else if ( 'n' == *p )
				{
					putchar( '\n' );
					sfmt = p + 1;
				}
				else
				{
					sfmt = p;
				}

				got_escape_char = false;
			}
		}

		if ( p > sfmt )
			printf( "%.*s", (int)(p - sfmt), sfmt );

		res = true;
	}

	return res ? result = true_atom, Error_OK : Error_Args;
}

EError builtin_slurp( const SCell& args, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	if ( !is_string(car(args)) )
		return Error_MissType;

	unsigned long read = 0;

	const char* text = slurp( car(args).value.string, &read );

	if ( text )
	{
		result = make_string( text, read );

		delete [] text;
	}
	else
	{
		result = nil;
	}

	return Error_OK;
}
