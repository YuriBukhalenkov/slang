#ifndef INOUT_HEADER
#define INOUT_HEADER

char* slurp     ( const char* path, unsigned long* read = NULL );
void  print_expr( const SCell& x );

EError builtin_print( const SCell& args, SCell& result );
EError builtin_slurp( const SCell& args, SCell& result );

#endif /* INOUT_HEADER */