#!/bin/bash

source ../VERSION

VERSION_BUILD_H="version_build.h"

rm -f $VERSION_BUILD_H

REVISION=$(git rev-parse --short HEAD)
BUILD_NUMBER=$(git rev-list --count HEAD)

cat > $VERSION_BUILD_H << EOF
#ifndef VERSION_BUILD_H
#define VERSION_BUILD_H

#define VERSION_NUMBER "${VERSION}"
#define VERSION_BUILD_DATE "$(date +%d.%m.%Y)"
#define VERSION_REVISION "$REVISION"
#define VERSION_BUILD "$BUILD_NUMBER"

#endif
EOF