#include <stdio.h>
#include <string.h>
#include <sys/errno.h>
#include <time.h>
#include "slang.h"
#include "parser.h"
#include "env.h"
#include "builtin.h"
#include "inout.h"
#include "version_build.h"

#define INPUT_BUFF (128U)

static void print( const SCell& expr, const char* prefix = NULL );
static void print_error( const EError err );

EError builtin_parse_csv( const SCell& args, SCell& result );


bool load_lib( SCell& env, const char* path )
{
	printf( "Reading %s:", path );

	const char* text = slurp( path );

	if ( text )
	{
		bool res = true;

		SCell expr, result;

		for ( const char* p = text; res && Error_OK == read_expr(p, &p, expr); )
		{
			const EError err = eval_expr( expr, env, result );

			if ( Error_OK != err )
			{
				print_error( err );
				print( expr, "Error in expression:\n\t" );
			}

			res &= Error_OK == err;
		}

		delete [] text;

		if ( res )
			puts( "[OK]" );
	}
	else
	{
		puts(strerror(errno));
	}

	return NULL != text;
}

static inline void print( const SCell& expr, const char* prefix /* = NULL */ )
{
	if ( prefix )
		fputs( prefix, stdout );

	print_expr( expr );
	putchar   ( '\n' );
}

static inline void print_error( const EError err )
{
	switch ( err )
	{
	case Error_OK      : puts( "OK" );             break;
	case Error_Syntax  : puts( "Syntax Error"   ); break;
	case Error_Unbound : puts( "Symbol unbound" ); break;
	case Error_Args    : puts( "Wrong args amount");break;
	case Error_MissType: puts( "Wrong type"     ); break;
	case Error_Bound   : puts( "Already bound"  ); break;
	}
}

static EError builtin_unixtime( const SCell& args, SCell& result )
{
	return is_nil(args)
		? result = make_num( make_num((long)time(NULL)) ), Error_OK
		: Error_Args;
}

static inline EError add( const char* name, const SCell& item, SCell& env )
{
	return env_set( env, make_atom(name), item );
}

static inline EError add( const char* name, Builtin func, SCell& env )
{
	return add( name, make_builtin(func), env );
}

/* register builtins */
static void add_builtins( SCell& env )
{
	add( "#t",   true_atom,    env );
	add( "#f",   false_atom,   env );
	add( "nil",  nil,          env );

	add( "car",  builtin_car,  env );
	add( "cdr",  builtin_cdr,  env );
	add( "cons", builtin_cons, env );

	add( "length",builtin_length,env );
	add( "apply", builtin_apply, env );

	add( "+",    builtin_add,  env );
	add( "-",    builtin_sub,  env );
	add( "*",    builtin_mul,  env );
	add( "/",    builtin_div,  env );
	add( "%",    builtin_rem,  env );
	add( "=",    builtin_numeq, env );
	add( "<",    builtin_numlt, env );
	add( ">",    builtin_numgt, env );
	add( "<=",   builtin_numle, env );
	add( ">=",   builtin_numge, env );

	add( "list?", builtin_is_list,  env );
	add( "nil?",  builtin_is_nil,   env );
	add( "atom?", builtin_is_atom,  env );
	add( "fun?",  builtin_is_fun,   env );
	add( "num?",  builtin_is_num,   env );
	add( "int?",  builtin_is_int,   env );
	add( "real?", builtin_is_real,  env );
	add( "pair?", builtin_is_pair,  env );
	add( "str?",  builtin_is_string,env );
	add( "macro?",builtin_is_macro, env );
	add( "eq?",   builtin_is_eq,    env );
	add( "lt?",   builtin_is_lt,    env );

	add( "now", builtin_unixtime, env );

	add( "print",    builtin_print,     env );
	add( "slurp",    builtin_slurp,     env );
	add( "csv->list",builtin_parse_csv, env );

	add( "read-expr", builtin_read_expr, env );
}


/* main function implementation */
int main( int argc, const char* argv[] )
{
	printf( "slang version: %s.%s, built: %s, revision: %s\n", 
				VERSION_NUMBER, VERSION_BUILD,
				VERSION_BUILD_DATE,
				VERSION_REVISION );

	SCell env = env_create( nil );

	/* Setup initial environment */
	add_builtins( env );
	load_lib( env, "lib/core.slang" );

	/* Loading extra modules */
	for ( int i = 1; i < argc; ++i )
	{
		load_lib( env, argv[i] );
	}

	char input[INPUT_BUFF];

	while ( NULL != fgets(input, sizeof(input), stdin) )
	{
		if ( 0 == strncmp("quit", input, 4) )
			break;

		if ( '\n' == input[0] )
			continue;

		const char* p = input;
		SCell       expr, result;
		EError      err = read_expr( p, &p, expr );

		if ( Error_OK == err )
			err = eval_expr( expr, env, result );

		if ( Error_OK == err )
			print( result );
		else
		{
			print_error( err );
			print( expr, "Error in expression:\n\t" );
		}
	}

	return 0;
}
