#include <math.h>
#include "slang.h"

typedef double (*math_fn_a1)(double);
typedef double (*math_fn_a2)(double, double);


//=====================================
// math builtins implementation

static EError num_math( const SCell& args, SCell& result, math_fn_a1 fn )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	return is_num(car(args))
		? result = make_num(make_num((*fn)(num_as_float(car(args).value.num)))), Error_OK
		: Error_MissType;
}

static EError num_math( const SCell& args, SCell& result, math_fn_a2 fn )
{
	if ( is_nil(args) || is_nil(cdr(args)) || !is_nil(cddr(args)) )
		return Error_Args;

	double p1 = num_as_float(car(args).value.num),
		   p2 = num_as_float(cadr(args).value.num);

	return is_num(car(args))
		? result = make_num(make_num((*fn)(p1, p2))), Error_OK
		: Error_MissType;
}

extern "C" EError builtin_cos( const SCell& args, SCell& result )
{
	return num_math( args, result, &cos );
}

extern "C" EError builtin_sin( const SCell& args, SCell& result )
{
	return num_math( args, result, &sin );
}

extern "C" EError builtin_tan( const SCell& args, SCell& result )
{
	return num_math( args, result, &tan );
}

extern "C" EError builtin_ln( const SCell& args, SCell& result )
{
	return num_math( args, result, &log );
}

extern "C" EError builtin_lg( const SCell& args, SCell& result )
{
	return num_math( args, result, &log10 );
}

extern "C" EError builtin_log2( const SCell& args, SCell& result )
{
	return num_math( args, result, &log2 );
}

extern "C" EError builtin_sqrt( const SCell& args, SCell& result )
{
	return num_math( args, result, &sqrt );
}

extern "C" EError builtin_pow( const SCell& args, SCell& result )
{
	return num_math( args, result, &pow );
}
