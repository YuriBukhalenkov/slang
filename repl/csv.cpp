#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

/* detect number, string or NIL */
static EError parse_field( const char* start, const char*& end, SCell& cell )
{
	EError err = Error_OK;

	/* quoted string */
	if ( '\"' == *start )
	{
		if ( NULL != (end = strchr(start + 1, '\"')) )
		{
			cell = make_string( start + 1, end - start - 1 );
			++end;
		}
		else
		{
			err = Error_Syntax;
		}
	}
	/* NIL */
	else if ( ',' == *start )
	{
		cell = nil;
		end = start;
	}
	else
	{
		end = strpbrk(start, ",\n\r");

		if ( NULL == end )
			end = start + strlen(start);

		/* string */
		if ( isalpha(*start) )
		{
			cell = make_string( start, end - start );
		}
		/* maybe a number or string started with digit */
		else if ( isdigit(*start) )
		{
			char *p;
			long ival = strtol( start, &p, 10 );
			if ( p == end )
			{
				cell = make_num( make_num(ival) );
			}
			else
			{
				double fval = strtod( start, &p );
				if ( p == end )
				{
					cell = make_num( make_num(fval) );
				}
				else
				{
					cell = make_string( start, end - start );
				}
			}
		}
	}

	return err;
}

EError builtin_parse_csv( const SCell& args, SCell& result )
{
	if ( is_nil(args) || !is_nil(cdr(args)) )
		return Error_Args;

	if ( !is_string(car(args)) )
		return Error_MissType;

	EError err = Error_OK;

	const char* head = car(args).value.string;
	const char* end  = head + strlen(head);

	SCell field, row = nil, csv = nil;

	for ( const char* tail = NULL; head && (Error_OK == err) && (head < end); )
	{
		if ( head && (Error_OK == (err = parse_field(head, tail, field))) )
		{
			row = cons(field, row);

			if ( '\n' == *tail )
			{
				csv = cons( list_reverse(row), csv );
				row = nil;
			}

			head = tail + 1;
		}
	}

	if ( Error_OK == err )
	{
		result = list_reverse( cons(list_reverse(row), csv) );
	}

	return err;
}