#!/bin/bash

os="unknown"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    os="linux"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    os="macosx"
fi

premake4 --file=premake.lua --cc=gcc --platform=x64 --os=$os gmake