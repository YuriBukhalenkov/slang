solution "slang"
  configurations { "Debug", "Release" }

  configuration { "Debug" }
     targetdir "build/Debug"

  configuration { "Release" }
    targetdir "build/Release"

  configuration { "linux", "gmake" }
    buildoptions { "-Wreturn-type" }


-- slang library
project "slang"
  location( project().name )
  
  language "C++"
  kind     "StaticLib"

  includedirs { "slang" }

  files{ "slang/*.h", "slang/*.cpp" }

  configuration { "Debug" }
    defines { "_DEBUG", "DEBUG" }
    flags   { "Symbols" }
    objdir  ( "build/" .. project().name .. "/Debug" )

  configuration { "Release" }
    defines { "NDEBUG" }
    flags   { "Optimize" }
    objdir  ( "build/" .. project().name .. "/Release" )


-- repl project
project "repl"
  location( project().name )

  language "C++"
  kind     "ConsoleApp"
  links    { "slang" }

  includedirs { "repl", "slang" }

  files { "repl/*.h", "repl/*.cpp" }
  excludes{ "version_build.h" }

  prebuildcommands { "./prebuild.sh" }

  configuration { "Debug" }
    defines { "_DEBUG", "DEBUG" }
    flags   { "Symbols" }
    objdir  ( "build/" .. project().name .. "/Debug" )

  configuration { "Release" }
    defines { "NDEBUG" }
    flags   { "Optimize" }
    objdir  ( "build/" .. project().name .. "/Release" )
